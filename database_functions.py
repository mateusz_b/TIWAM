# -*- coding: utf-8 -*-
import argparse
import sqlite3
import hashlib
from collections import namedtuple

__author__ = 'mateuszb'

user = namedtuple("User", ['username', 'password'])


class DatabaseConnection:
    def __init__(self):
        self.connection = sqlite3.connect("users.db")
        self.cursor = self.connection.cursor()


def create_users_table(cur):
    try:
        cur.execute("CREATE TABLE users (username text, password text);")
    except sqlite3.OperationalError:
        print("Table already exists")


def create_user(cur, user, password):
    password = hashlib.sha1(password.encode("utf-8")).hexdigest()
    cur.execute(
        "INSERT INTO users('username', 'password') VALUES (?, ?)",
        (user, str(password)))


def get_user(cur, user):
    params = (user,)
    user = cur.execute("SELECT * FROM users WHERE username=?", params)
    return user


def get_all_users(cur):
    cur.execute("SELECT username FROM users")
    return cur.fetchall()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--username', default='admin')
    parser.add_argument('--password', default='password')
    args = parser.parse_args()
    dc = DatabaseConnection()
    create_users_table(dc.cursor)
    create_user(dc.cursor, args.username, args.password)
    dc.connection.commit()
    get_user(dc.cursor, args.username)
    u = user(*dc.cursor.fetchone())
    print("Created username")
    print("username: ", u.username)
    print("Hashed password: ", u.password)
