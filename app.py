# -*- coding: utf-8 -*-
import hashlib
import sqlite3

from flask import Flask, render_template, request, session, g

from database_functions import get_user, user
from utils import generate_challenge_key

__author__ = 'mateuszb'

app = Flask(__name__, static_path='/static')

DATABASE = 'users.db'


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.context_processor
def inject_challenge_key():
    session["challenge_key"] = generate_challenge_key()
    return dict(challenge_key=session["challenge_key"])


@app.route('/')
def home():
    return render_template("index.html")


@app.route('/login', methods=["POST"])
def login():
    username = request.form["username"]
    password = request.form["password"]
    challenge_key = session["challenge_key"]
    received_challenge_key = request.form["challenge_key"]
    if received_challenge_key == challenge_key:
        print("Challenge keys match")
    else:
        print("Challenge keys don't match")
        return "invalid credentials", 401
    cur = get_db().cursor()
    get_user(cur, username)
    fetched_user = cur.fetchone()
    if fetched_user is None:
        print("User not found")
        return "invalid credentials", 401
    db_user = user(*fetched_user)
    db_password = db_user.password
    sha1 = hashlib.sha1(db_password.encode('utf-8') +
                        challenge_key.encode('utf-8')).hexdigest()
    if sha1 == password:
        print("Correct password")
    else:
        print("Incorrect password")
        return "invalid credentials", 401
    return "user logged in", 200


if __name__ == '__main__':
    # set the secret key.  keep this really secret:
    app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    app.run(debug=True)
