Przygotowanie środowiska:

- (opcjonalnie) utworzenie środowiska wirtualnego dla projektu (virtualenv) 
z interpreterem python3
- zainstalowanie zależności
`pip install -r requirements.txt`
- stworzenie bazy i dodanie użytkownika
`python3 database_functions.py --username user --password pass`
- uruchomienie serwera
`python3 app.py`
